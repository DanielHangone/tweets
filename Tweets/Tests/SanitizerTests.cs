﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tweets.Tweeter.Behaviour;

namespace Tests
{
    /// <summary>
    /// Unit Tests only added for this one method due to time constraints.
    /// Unit Test should provide a wide varienty of negative and positive testing.
    /// Unit Tests should ideally be written once the method signature is decided upon and befor the method body is written.
    /// /To run these tests in VS build the solution and then click Tests, Run, All Tests
    /// </summary>
    [TestClass]
    public class SanitizerTests
    {
        private IInputSanitizer inputSanitizer;

        [TestInitialize()]
        public void Startup()
        {
            inputSanitizer = new InputSanitizer();
        }

        [TestMethod]
        public void IsArgumentsValidNotNull()
        {
            //Arrange             
            string[] parameters = null;
            var expectedResponse = false;

            //Act
            var actualResponse = inputSanitizer.IsArgumentsValid(parameters);

            //Assert
            Assert.AreEqual(expectedResponse, actualResponse);
        }

        [TestMethod]
        public void IsArgumentsValidZeroParams()
        {
            //Arrange             
            var parameters = new string[0];
            var expectedResponse = false;

            //Act
            var actualResponse = inputSanitizer.IsArgumentsValid(parameters);

            //Assert
            Assert.AreEqual(expectedResponse, actualResponse);
        }

        [TestMethod]
        public void IsArgumentsValidOneParams()
        {
            //Arrange             
            var parameters = new string[1] { "Test.txt"};
            var expectedResponse = false;

            //Act
            var actualResponse = inputSanitizer.IsArgumentsValid(parameters);

            //Assert
            Assert.AreEqual(expectedResponse, actualResponse);
        }

        [TestMethod]
        public void IsArgumentsValidTwoParams()
        {
            //Arrange             
            var parameters = new string[2] { "Test.txt", "Test.txt" };
            var expectedResponse = true;

            //Act
            var actualResponse = inputSanitizer.IsArgumentsValid(parameters);

            //Assert
            Assert.AreEqual(expectedResponse, actualResponse);
        }

        [TestMethod]
        public void IsArgumentsValidThreeParams()
        {
            //Arrange             
            var parameters = new string[3] { "Test.txt", "Test.txt", "Test.txt"};
            var expectedResponse = false;

            //Act
            var actualResponse = inputSanitizer.IsArgumentsValid(parameters);

            //Assert
            Assert.AreEqual(expectedResponse, actualResponse);
        }

        public void IsArgumentsValidTwoEmptyParams()
        {
            //Arrange             
            var parameters = new string[2] { "", "" };
            var expectedResponse = false;

            //Act
            var actualResponse = inputSanitizer.IsArgumentsValid(parameters);

            //Assert
            Assert.AreEqual(expectedResponse, actualResponse);
        }

        public void IsArgumentsValidTwoWhitespaceParams()
        {
            //Arrange             
            var parameters = new string[2] { " ", " " };
            var expectedResponse = false;

            //Act
            var actualResponse = inputSanitizer.IsArgumentsValid(parameters);

            //Assert
            Assert.AreEqual(expectedResponse, actualResponse);
        }

        public void IsArgumentsValidTwoParamsOneWhitespace()
        {
            //Arrange             
            var parameters = new string[2] { " ", "" };
            var expectedResponse = false;

            //Act
            var actualResponse = inputSanitizer.IsArgumentsValid(parameters);

            //Assert
            Assert.AreEqual(expectedResponse, actualResponse);
        }
    }
}
