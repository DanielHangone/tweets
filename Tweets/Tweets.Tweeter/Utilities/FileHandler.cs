﻿using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Tweets.Tweeter.Utilities
{
    public class FileHandler : IFileHandler
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public IEnumerable<string> GetLines(string filePath)
        {
            var lines = new List<string>();
            try
            {
                if (IsValidFilePath(filePath))
                {
                    lines = GetLinesFromValidPath(filePath).ToList();
                }
                else
                {
                    logger.Warn(string.Format("GetLines: Invalid filePath=[{0}]", filePath));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error reading file at {0}", filePath);
                logger.Error(e, string.Format("GetLines: Cannot get lines from filePath=[{0}]", filePath));
                throw e;
            }
            return lines;
        }

        private IEnumerable<string> GetLinesFromValidPath(string filePath)
        {
            return File.ReadAllLines(filePath, Encoding.ASCII);
        }

        private bool IsValidFilePath(string filePath)
        {
            return File.Exists(filePath);
        }
    }
}
