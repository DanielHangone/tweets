﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tweets.Tweeter.Utilities
{
    interface IFileHandler
    {
        IEnumerable<string> GetLines(string filePath);
    }
}
