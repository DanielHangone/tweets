﻿using System.Collections.Generic;
using Tweets.Models;

namespace Tweets.Tweeter.Utilities
{
    interface IWriter
    {
        void PrintMessages(IEnumerable<FollowedTweets> userAccounts);
    }
}
