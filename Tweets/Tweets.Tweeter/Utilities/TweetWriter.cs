﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using Tweets.Models;
using Tweets.Tweeter.Utilities;

namespace Tweets.Tweeter.Behaviour
{
    class TweetWriter : IWriter
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public void PrintMessages(IEnumerable<FollowedTweets> userAccounts)
        {
            try
            {
                foreach (var userAccount in userAccounts.OrderBy(o => o.User.Name))
                {
                    Console.WriteLine(userAccount.User.Name);
                    foreach (var tweetedMessage in userAccount.TweetedMessages)
                    {
                        Console.WriteLine("\t@{0}: {1}", tweetedMessage.Sender.Name, tweetedMessage.Message);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error writing tweets");
                logger.Error(e, string.Format("PrintMessages: Error writing messages"));
                throw e;
            }
        }
    }
}
