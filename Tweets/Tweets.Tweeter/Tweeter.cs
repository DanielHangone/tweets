﻿using NLog;
using System;
using Tweets.Tweeter.Behaviour;
using Tweets.Tweeter.Utilities;

namespace Tweets.Tweeter
{
    class Tweeter
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        static void Main(string[] args)
        {
            try
            {
                IInputSanitizer inputSanitizer = new InputSanitizer();

                if (inputSanitizer.IsArgumentsValid(args))
                {
                    IFileHandler fileHandler = new FileHandler();
                    var usersLines = fileHandler.GetLines(args[0]);
                    var tweetsLines = fileHandler.GetLines(args[1]);

                    var users = inputSanitizer.GetUserAndFollowersFromFile(usersLines);
                    var tweets = inputSanitizer.GetTweetsFromFile(tweetsLines);

                    var tweetProcessor = new TweetProcessor(tweets, users);
                    var messages = tweetProcessor.Process();

                    IWriter tweetWriter = new TweetWriter();
                    tweetWriter.PrintMessages(messages);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("An Error has occured. Please make sure that your files are in place and that their contents adhears to the file specfications");
                logger.Error(e, "TweeterMain: A critical error has occured.");
            }
            Console.ReadKey();


            //Dependancy injection should be used here instead of creating concrete dependancies.
            //This would allow the components to be swapped out without without affecting the system as there are no inter dependancies.
            //I am however not familiar with using DI in a console app
        }
    }
}
