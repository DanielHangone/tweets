﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using Tweets.Models;

namespace Tweets.Tweeter.Behaviour
{
    public class InputSanitizer : IInputSanitizer
    {
        private const int TWEET_MAX_LENGTH = 140;
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public bool IsArgumentsValid(string[] arguements)
        {
            var result = true;
            if (arguements == null)
            {
                Console.WriteLine("There are no Arguemnts specified.");
                logger.Warn("IsArgumentsValid: Input Arguement = NULL");
                result = false;
            }
            else if (arguements.Length != 2)
            {
                Console.WriteLine("2 Arguments need to be specified: [user file] [tweets file]");
                logger.Warn("IsArgumentsValid: Input Arguement is not 2 parameters");
                result = false;
            }
            return result;
        }

        public IEnumerable<Tweet> GetTweetsFromFile(IEnumerable<string> tweetInputs)
        {
            var tweets = new List<Tweet>();
            try
            {
                foreach (var tweetInput in tweetInputs)
                {
                    if (string.IsNullOrWhiteSpace(tweetInput))
                    {
                        Console.WriteLine("There is blank line in the tweet file");
                        logger.Warn("GetTweetsFromFile: There is blank line in the tweet file");
                    }
                    else if (!tweetInput.Contains('>'))
                    {
                        Console.WriteLine("Each tweet input line must contain a > char");
                        logger.Warn(string.Format("GetTweetsFromFile: There is no > on tweet input line [{0}]", tweetInput));
                    }
                    var lineParts = tweetInput.Split('>');
                    if (lineParts.Count() == 2)
                    {
                        var author = new User(lineParts[0].Trim(), null);
                        var message = lineParts[1];
                        if (message.Length > TWEET_MAX_LENGTH)
                        {
                            Console.WriteLine("Max Char length exceeded on tweet:[{0}] Trimmed to {1} chars", message, TWEET_MAX_LENGTH);
                            logger.Warn(string.Format("GetTweetsFromFile: Max length of {1}chars exceeded on tweet input line [{0}]", tweetInput, TWEET_MAX_LENGTH));
                            message = message.Substring(0, TWEET_MAX_LENGTH);
                        }
                        tweets.Add(new Tweet(author, message));
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error getting tweets from file");
                logger.Error(e, string.Format("GetTweetsFromFile: ERROR parsingTweets"));
                throw;
            }
            return tweets;
        }

        public IEnumerable<User> GetUserAndFollowersFromFile(IEnumerable<string> userInputLines)
        {
            var users = new List<User>();
            try
            {
                foreach (var line in userInputLines)
                {
                    var words = line.Split(' ');
                    if (words != null && words.Length >= 3)
                    {
                        var name = words[0];
                        var followings = new List<User>();
                        foreach (var following in words.Skip(2))
                        {
                            followings.Add(new User(following.Trim(','), null));
                        }
                        users.Add(new User(name, followings));
                    }
                }
                var followingOnlyUsers = GetUsersNotFollowingButFollowed(users);
                users.AddRange(followingOnlyUsers);

            }
            catch (Exception e)
            {
                logger.Error(e, string.Format("GetUserAndFollowersFromFile: ERROR parsing users"));
                throw;   
            }
            return users.GroupBy(x=>x.Name)
                .Select(x=>x.FirstOrDefault());

        }

        private IEnumerable<User> GetUsersNotFollowingButFollowed(IEnumerable<User> users)
        {
            var newUsers = new List<User>();
            foreach (var user in users)
            {
                foreach (var following in user.Followings)
                {
                    if (!users.Any(x=>x.Name == following.Name))
                    {
                        newUsers.Add(new User(following.Name, new List<User>()));
                    }
                }
            }
            return newUsers;
        }
    }
}
