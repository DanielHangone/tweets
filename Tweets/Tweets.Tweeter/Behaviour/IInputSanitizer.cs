﻿using System.Collections.Generic;
using Tweets.Models;

namespace Tweets.Tweeter.Behaviour
{
    public interface IInputSanitizer
    {
        bool IsArgumentsValid(string[] arguements);
        IEnumerable<Tweet> GetTweetsFromFile(IEnumerable<string> tweetInputs);
        IEnumerable<User> GetUserAndFollowersFromFile(IEnumerable<string> userInputLines);
    }
}
