﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using Tweets.Models;

namespace Tweets.Tweeter.Behaviour
{
    class TweetProcessor
    {
        private readonly IEnumerable<Tweet> _tweets;
        private readonly IEnumerable<User> _users;
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public TweetProcessor(IEnumerable<Tweet> tweets, IEnumerable<User> users)
        {
            _tweets = tweets;
            _users = users;
        }

        /// <summary>
        /// This section should be done with pub/sub but the constraint of combing all user tweets per user and in alphabetical order 
        /// means that the subscriber events needs to be collated afterwards and ordered.
        /// 
        /// If the Tweets could be written to the console out of alpabetical order and each follower could write the message they received
        /// to the console as they received it then events could be implemented.
        /// 
        /// To scale this system, an NServiceBus implementation can be investigated.
        /// </summary>
        /// <returns></returns>

        internal IEnumerable<FollowedTweets> Process()
        {
            var followedTweets = new List<FollowedTweets>();
            try
            {
                foreach (var user in _users)
                {
                    var matchingTweet = _tweets.Where(x => user.Followings.Any(y => y.Name == x.Author.Name) || user.Name == x.Author.Name);

                    var tweetedMessages = matchingTweet.Select(x => new TweetedMessage(new User(x.Author.Name, null), x.Message));

                    followedTweets.Add(new FollowedTweets(new User(user.Name, null), tweetedMessages));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error processing tweets");
                logger.Error(e, string.Format("TweetProcessor: ERROR Processing Tweets"));
                throw;
            }
            return followedTweets;
        }
    }
}
