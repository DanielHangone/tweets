﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Tweets.Utilities
{
    public class FileHandler
    {
        public IEnumerable<string> GetLines(string filePath)
        {
            var lines = new List<string>();
            if (IsValidFilePath(filePath))
            {
                lines = GetLinesFromValidPath(filePath).ToList();
            }
            return lines;
        }

        private IEnumerable<string> GetLinesFromValidPath(string filePath)
        {
            return File.ReadAllLines(filePath, Encoding.ASCII);
        }

        private bool IsValidFilePath(string filePath)
        {
            return File.Exists(filePath);
        }
    }
}
