﻿namespace Tweets.Models
{
    public class TweetedMessage
    {
        public TweetedMessage(User sender, string message)
        {
            Sender = sender;
            Message = message;
        }

        public User Sender { get; private set; }
        public string Message { get; private set; }
    }
}
