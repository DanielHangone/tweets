﻿using System.Collections.Generic;

namespace Tweets.Models
{
    public class User
    {
        public User(string name, IEnumerable<User> followings)
        {
            Name = name;
            Followings = followings;
        }

        public string Name { get; private set; }
        public IEnumerable<User> Followings { get; private set; }
    }
}
