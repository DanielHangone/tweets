﻿namespace Tweets.Models
{
    public class Tweet
    {
        public Tweet(User author, string message)
        {
            Author = author;
            Message = message;
        }

        public User Author { get; private set; }
        public string Message { get; private set; }
    }
}
