﻿using System.Collections.Generic;

namespace Tweets.Models
{
    public class FollowedTweets
    {
        public FollowedTweets(User user, IEnumerable<TweetedMessage> tweetedMessages)
        {
            User = user;
            TweetedMessages = tweetedMessages;
        }

        public User User { get; private set; }
        public IEnumerable<TweetedMessage> TweetedMessages { get; private set; }
    }
}
